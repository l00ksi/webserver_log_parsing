require_relative './lib/pageSorter'
require_relative './lib/convertLogData'
require 'terminal-table'

class Parser

	def initialize(webserver_log)
		@webserver_log = webserver_log
		@unique_pages = []
		extracted_data = ConvertLogData.new(@webserver_log).extract_log_values
		@parsed_data = PageSorter.new(extracted_data["pages"], extracted_data["visits"])
	end

	def text_output_total_count
		table = Terminal::Table.new :headings => ['Page', 'Number of visits'], :rows => @parsed_data.get_sorted_pages
		puts table
	end

	def text_output_unique_count
		if @unique_pages.empty?
				@unique_pages = @parsed_data.get_sorted_unique_pages	# Caching unique pages because getting unique values is a bit slower operation,
		end																												# so in case if user repeateadly calls this function (from instance of 'Parser' class),
																															# it will be slow for the first time but faster after that

		table = Terminal::Table.new :headings => ['Page', 'Number of unique visits'], :rows => @unique_pages
		puts table
	end

	def get_most_popular_page
		@parsed_data.get_sorted_pages[0][0]
	end

	def get_most_frequent_visitor
		@parsed_data.get_sorted_visits[0][0]
	end

end


if (ARGV.length > 0)
	parser = Parser.new(ARGV[0])
	parser.text_output_total_count
	parser.text_output_unique_count
end
