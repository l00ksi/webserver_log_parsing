This script is made for counting number of total visits and number of unique visits to the each of server's pages.

Requirement for running script:

$ bundle install

-> This will install - gem "terminal-table"
which is used to make the output of the script prettier in the console.


Run tests to make sure everything is working:
$ ruby tests/parserTest.rb


To get output to the console,
run:

$ ruby parser.rb webserver.log

The output should be formatted as a table with two columns, one representing pages and one representing number of visits to the page.



Additionally it is possible to import it within interactive ruby with:

$ require 'path_to/file/parser'
$ require_relative 'parser' # If you are in the same directory

Script requires webserver.log file input for initialization. (Provided in the project directory)	

irb console Example:

	require_relative 'parser'	# If you are in the same directory

	parser = Parser.new("webserver.log")

	parser.text_output_total_count # returns the table with two columns(pages, number of visits)
	parser.text_output_unique_count # returns the table with two columns(pages, number of unique visits)

	parser.get_most_popular_page # as the name says, it will return the most visited page
	parser.get_most_frequent_visitor # this will return the most frequent IP Address visiting the page
