class ConvertLogData

	def initialize(input_file)
		if Dir.pwd.split("/")[-1] != "webserver_log_parsing"
			Dir.chdir("./..")
		end

		if !File.exists? input_file
			raise IOError, "File not found!"
		end
		@input_file = input_file
	end

	def extract_log_values
		output = {"pages" => [], "visits" => []}
			File.open(@input_file, "r") do |file|
				file.each do |line|
					output["pages"] << line.split(" ")[0]
					output["visits"] << line.split(" ")[1]
				end
			end 
		return output
	end

end