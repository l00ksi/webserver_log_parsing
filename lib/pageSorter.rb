class PageSorter

	def initialize(pages, visits=nil)
		@pages = pages
		@visits = visits
	end

	def get_sorted_pages
		sort_values(@pages)
	end

	def get_sorted_visits
		if not @visits.nil?
			sort_values(@visits)
		end
	end

	def get_sorted_unique_pages
		if not @visits.nil?
			sort_unique_pages(@pages, @visits)
		end
	end

	def get_all_pages
		@pages
	end

	private

	def sort_values(values)
		sorted_values = {}

		values.each do |value|
			sorted_values[value] = values.count(value)
		end

		sort_descending = -1
		return sorted_values.sort_by { |value, occurence| occurence * sort_descending }
	end

	def sort_unique_pages(pages, visits)
		unique_sorted_pages = {}

		pages.each do |page|
			page_indexes = pages.each_index.select { |index| pages[index] == page }		# Finding all indexes of specific page that will be used to match them with IP Adresses
			page_visits = []
			visits.each_with_index do |visit, index|
				if page_indexes.include? index
					page_visits << visit
				end
			end
			unique_sorted_pages[page] = page_visits.uniq.count
		end

		sort_descending = -1
		return unique_sorted_pages.sort_by { |page, visits| visits * sort_descending }
	end

end
