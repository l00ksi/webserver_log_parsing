require "test/unit"
require_relative './../parser'
require_relative './../lib/convertLogData'
require_relative './../lib/pageSorter'

class ParserTest < Test::Unit::TestCase
	
	def setup
		@test_file = "webserver.log"
		@parser = Parser.new(@test_file)

		extracted_data = ConvertLogData.new(@test_file).extract_log_values
		@pages = extracted_data["pages"]
		@visits = extracted_data["visits"]
	end
	
	def test_file_length
		pages_size = PageSorter.new(@pages, @visits).get_all_pages.size
		assert_equal 500, pages_size
	end	

	def test_most_viewed_page
		assert_equal "/about/2", @parser.get_most_popular_page
	end

	def test_unique_visits
		assert_equal 23, @visits.uniq.count
	end

	def test_visitor_address_length
		assert_not_equal true, @visits[0].length > 15
	end

	def test_most_frequent_visitor
		visitor = @parser.get_most_frequent_visitor
		assert_equal "722.247.931.582", visitor
	end

	def test_parser_wrong_file
		assert_raise IOError do 
			parser = Parser.new("ghost_file.log")
			parser.get_most_popular_page
		end 
	end

	def test_extracting_values_from_log
		test_file = 'webserver.log'
		output = ConvertLogData.new(test_file).extract_log_values

		assert_equal "/help_page/1", output["pages"][0]
		assert_equal "/about", output["pages"][15]
		assert_equal "126.318.035.038", output["visits"][0]
		assert_equal "126.318.035.038", output["visits"][15] 
	end

	def test_sorting_values
		pages = @pages[50...100]
		visits = @visits[50...100]	

		sorted_values = PageSorter.new(pages, visits)
	
		sorted_pages = sorted_values.get_sorted_pages
		sorted_visits = sorted_values.get_sorted_visits

		assert_equal "/about/2", sorted_pages[1][0]
		assert_equal 2, sorted_visits[12][1]		

	end

	def test_sort_unique_pages
		sorted_values = PageSorter.new(@pages, @visits)

		sorted_unique_pages = sorted_values.get_sorted_unique_pages
	
		assert_equal "/help_page/1", sorted_unique_pages[0][0]
	end

end